# frozen_string_literal: true

require 'test_helper'

class BoxOfficeControllerTest < ActionDispatch::IntegrationTest
  test 'should get box office front-page' do
    get root_url
    assert_response :success
  end
end

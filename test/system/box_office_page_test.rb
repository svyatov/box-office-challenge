# frozen_string_literal: true

require 'application_system_test_case'

class BoxOfficePageTest < ApplicationSystemTestCase
  ROWS_SELECTOR = '#tickets > tbody > tr'

  test 'Next day button' do
    visit root_url

    rows = page.all ROWS_SELECTOR
    assert_equal 'Dear Evan Hansen 30 10', rows[0].text
    assert_equal 'The Lion King 5 25', rows[1].text
    assert_equal 'Backstage access to Wicked 15 15', rows[2].text
    assert_equal 'Hamilton 0 80', rows[3].text

    click_button 'Next day'

    rows = page.all ROWS_SELECTOR
    assert_equal 'Dear Evan Hansen 29 11', rows[0].text
    assert_equal 'The Lion King 4 24', rows[1].text
    assert_equal 'Backstage access to Wicked 14 16', rows[2].text
    assert_equal 'Hamilton 0 80', rows[3].text

    1.upto(15) { click_button 'Next day' }

    rows = page.all ROWS_SELECTOR
    assert_equal 'Dear Evan Hansen 14 26', rows[0].text
    assert_equal 'The Lion King -11 0', rows[1].text
    assert_equal 'Backstage access to Wicked -1 0', rows[2].text
    assert_equal 'Hamilton 0 80', rows[3].text
  end
end

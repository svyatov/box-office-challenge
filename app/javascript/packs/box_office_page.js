import Ticket from './Ticket'
import BoxOffice from './BoxOffice'

const printOutTickets = (table, tickets) => {
  const tableBody = table.querySelector('tbody')
  const newTableBody = document.createElement('tbody')

  tickets.forEach(ticket => {
    const row = document.createElement('tr')
    row.innerHTML = `<td>${ticket.showTitle}</td><td>${ticket.sellIn}</td><td>${ticket.value}</td>`
    newTableBody.appendChild(row)
  })

  tableBody.replaceWith(newTableBody)
}

window.addEventListener('DOMContentLoaded', () => {
  const tickets = [
    new Ticket('Dear Evan Hansen', 30, 10),
    new Ticket('The Lion King', 5, 25),
    new Ticket('Backstage access to Wicked', 15, 15),
    new Ticket('Hamilton', 0, 80)
  ]
  const boxOffice = new BoxOffice(tickets)
  const table = document.getElementById('tickets')
  const nextDayBtn = document.getElementById('next-day-btn')

  printOutTickets(table, boxOffice.tickets)

  nextDayBtn.addEventListener('click', () => {
    printOutTickets(table, boxOffice.updateValues())
  })
})

export default class BoxOffice {
  constructor(tickets = []) {
    this.tickets = tickets
  }

  updateValues() {
    for (let i = 0; i < this.tickets.length; i++) {
      if (
        this.tickets[i].showTitle != 'Dear Evan Hansen' &&
        this.tickets[i].showTitle != 'Backstage access to Wicked'
      ) {
        if (this.tickets[i].value > 0) {
          if (this.tickets[i].showTitle != 'Hamilton') {
            this.tickets[i].value = this.tickets[i].value - 1
          }
        }
      } else {
        if (this.tickets[i].value < 50) {
          this.tickets[i].value = this.tickets[i].value + 1
          if (this.tickets[i].showTitle == 'Backstage access to Wicked') {
            if (this.tickets[i].sellIn < 11) {
              if (this.tickets[i].value < 50) {
                this.tickets[i].value = this.tickets[i].value + 1
              }
            }
            if (this.tickets[i].sellIn < 6) {
              if (this.tickets[i].value < 50) {
                this.tickets[i].value = this.tickets[i].value + 1
              }
            }
          }
        }
      }
      if (this.tickets[i].showTitle != 'Hamilton') {
        this.tickets[i].sellIn = this.tickets[i].sellIn - 1
      }
      if (this.tickets[i].sellIn < 0) {
        if (this.tickets[i].showTitle != 'Dear Evan Hansen') {
          if (this.tickets[i].showTitle != 'Backstage access to Wicked') {
            if (this.tickets[i].value > 0) {
              if (this.tickets[i].showTitle != 'Hamilton') {
                this.tickets[i].value = this.tickets[i].value - 1
              }
            }
          } else {
            this.tickets[i].value = this.tickets[i].value - this.tickets[i].value
          }
        } else {
          if (this.tickets[i].value < 50) {
            this.tickets[i].value = this.tickets[i].value + 1
          }
        }
      }
    }

    return this.tickets
  }
}

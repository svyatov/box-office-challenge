export default class Ticket {
  constructor(showTitle, sellIn, value) {
    this.showTitle = showTitle
    this.sellIn = sellIn
    this.value = value
  }
}

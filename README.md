# Box Office Challenge

This is a bare-bones Rails 5.1 app that we've put together to see how you think.

We hesitate even to call it a "challenge" or a "test"
because this isn't designed to stump you.
We just want to see what your instincts are
with respect to applying Rails' and ES6 abstractions
to a specific and realistic problem.

Before you start make sure you have `yarn` installed on your machine.

So without further ado...

Please begin by running setup script:

```
bin/setup
```

This should install everything you need.

To start the application use this command:

```
bin/start
```

After that you can access the app here: `http://localhost:5000`

## Task #1

Here is the imaginary scenario:

We are a small ticket broker with a prime location in
NYC ran by a friendly manager named John. We buy and sell only the best tickets.
Unfortunately, most of our tickets are constantly degrading in value. 

We have a system in place that updates our inventory for us. It was developed by a developer named
Leeroy, who is no longer on the team. Your task is to add a couple of new features to our system so that
we can begin selling tickets to a new show. 

A quick intro to our system:

- All tickets have a `sellIn` property which denotes the number of days we have to sell the ticket
- All tickets have a `value` property which denotes how valuable the ticket is
- At the end of each day our system lowers both values for every ticket, except as noted below

Pretty simple, right? Well this is where it gets interesting:

- Once the sell by date has passed, `value` degrades twice as fast
- The `value` of a ticket is never negative
- Tickets to "Dear Evan Hansen" actually increase in `value` the older they get
- The `value` of a ticket is never more than 50, except for "Hamilton", which always has a ticket value of 80.
- Tickets to "Hamilton", which always has extreme demand, never have to be sold and never decrease in `value`
- Tickets to "Backstage access to Wicked", like tickets to "Dear Evan Hansen", increase in `value` as its `sellIn` value approaches;
  `value` increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
  `value` drops to 0 when `sellIn` is below zero.

We have recently signed a supplier of hot tickets to "Hello, Dolly!".
This requires an update to our system:

- Tickets to "Hello, Dolly!" degrade in `value` twice as fast as normal tickets

Feel free to make any changes to the `BoxOffice#updateValues` (`app/javascript/packs/BoxOffice.js`) method
and add any new code as long as everything still works correctly.
However, do not alter the `Ticket` class (`app/javascript/packs/Ticket.js`) or its properties.

As you'll see the current `BoxOffice#updateValues` method implementation is very messy and error-prone.
Our intention here is not only to see that you can understand the code well, but also to see
how you'd write this logic yourself in a proper, clean, readable and maintainable manner.
In other words, this task is all about refactoring, and not about inserting a couple more `if`s in the right place.

You'll find some initial tests in `test/system/box_office_page.test.rb` to help you start.
These tests don't cover the code for 100%. You will need to add more tests to make sure your code is rock-solid.
Feel free to add "native" JS tests using any JS testing framework if you don't want to use `Capybara`.

**Show us the code you can say you're trully proud of!**

## Task #2

Currently we have our tickets hardcoded inside the `app/javascript/packs/box_office_page.js` file.
It was OK at the beginning but now we'd like to add some sort of admin page to manage them (create, read, update, delete).
Use any database engine you like for this. No extra design is needed, keep it simple.
Rails scaffolding feature is totally fine, although keep in mind requirements for tickets and add proper validations.

## Task #3

Using existing HTML markup for the "Next day" button make it look like this:

![Button](the-button.png)

Screenshot is taken from a retina display, so ignore the size.  The actual button should be about half as big.

Everything you need is inside the `app/assets/stylesheets/button.scss` file.

Only the initial button state is required, ignore `hover`, `active`, `focus` or any other states. All gradients are linear.
You must put your code inside `.button` class in place of `// TODO` comment.
You must **not** change markup or redefine Bootstrap classes in any way.

## Result

If all 3 tasks are done properly, the app should work like this:

1. going to `localhost:5000` in a browser presents the tickets stored inside the database
2. tickets can be managed on a separate page
3. clicking "Next day" button applies rules described in task #1 and updates ticket's table with new values (on the front-end only, it does not update anything in the database)
4. "Next day" button looks like the screenshot of "Test" button in task #3
5. running `bin/rake` runs all the tests without any errors

### !!! Please, don't send pull requests to this repository !!!

Instead, please pull tar everything together and send it to tom@show-score.com